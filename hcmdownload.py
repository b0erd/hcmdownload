### HCM documents downloader version 0.1
#   intended for Firefox on Linux/Unix-like operating systems
#
# Please take this script without warranty of any kind.
# If you don't understand what it does, please don't execute it.
# This script will remove files on your computer.
# Please don't blame me if it erases the wrong ones.
# This script will autonomously log in to the HCM portal.
# As you are liable for your account and the actions originating
# from your computer and network, please check the legal
# situation and applicable contracts before running it.
#
# I am not responsible for any harm this does to you or your computer.
#
# License: http://www.gnu.org/licenses/gpl-3.0.txt
#
# This is a python program. To learn more about python
# look here: https://www.python.org/
# The program works like a puppeteer on Firefox. In order
# to perform the automation it uses the testing framework
# Selenium: https://selenium-python.readthedocs.io/
#
# To make it work please install
# Python 3
# Selenium
#
# For Debian, Ubuntu etc.:
# apt update
# apt install python3 python3-selenium firefox-geckodriver
#
# For Arch, Manjaro, EndeavourOS:
# pacman -Sy
# pacman -S python python-selenium geckodriver
#
# You will have to fill the variables with your
# credentials before running the script.
#
#
#
# To run execute:
# python3 hcmdownload.py




#from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import datetime
import os
import random

# Please fill in these variables
downloadbacktodate = '01.02.2021' # in the format DD.MM.YYYY, according to Erstelldatum
username = 'username' # your HCM username
password = 'password' # your HCM password, please don't share this with colleagues
downloadfolder = os.path.expanduser('~') + '/Downloads/' # your download folder

try:
    oldesterstelldatum = time.strptime(downloadbacktodate, '%d.%m.%Y')
except:
    oldesterstelldatum = datetime.date(1995, 3, 1)

# Change browser settings, so Firefox won't ask for download preferences every time
profile = webdriver.FirefoxProfile()
profile.set_preference("browser.download.dir", downloadfolder)
profile.set_preference("browser.download.folderList", 2)
#profile.set_preference("browser.download.manager.showWhenStarting", False)
profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf")
profile.set_preference('pdfjs.disabled', True)

# Start browser
print('Opening page...')
driver = webdriver.Firefox(profile)
driver.get("https://easyjethcm.pi-asp.de/")
time.sleep(random.randint(3,5))

# If we are on the correct page look for login field
assert "easyJet Airline Company Limited" in driver.title
frames = driver.find_elements_by_tag_name('frame')
activeframe = driver.switch_to.frame(frames[0])
assert "Benutzername" in driver.page_source

elem = driver.find_element_by_name('login')
elem.send_keys(username)
elem = driver.find_element_by_name('passwd')
elem.send_keys(password)
elem.send_keys(Keys.RETURN)
time.sleep(random.randint(4,8))

# Search for the correct frame to navigate to Mitarbeiterdaten
driver.switch_to.default_content()
frames = driver.find_elements_by_tag_name('frame')
activeframe = driver.switch_to.frame(frames[0])
assert "Mitarbeiterdaten" in driver.page_source
print('I am logged in. Looking for documents...')
mitarbeiterdatenknopf = driver.find_element_by_link_text('Mitarbeiterdaten')
mitarbeiterdatenknopf.click()

# Scrape through the list of files
filenumber = 0
lastfilefound = False

while not lastfilefound:
    time.sleep(random.randint(1,3)) # wait to not overload the server
    # find the frame containing the table
    driver.switch_to.default_content()
    frames = driver.find_elements_by_tag_name('frame')

    activeframe = driver.switch_to.frame(frames[3])
    iframes = driver.find_elements_by_tag_name('iframe')
    activeframe = driver.switch_to.frame(iframes[0])
    assert 'fw_tvt_wpsfile' in driver.page_source
    # Is there a table row with the current filenumber?
    try:
        datumfeld = driver.find_element_by_css_selector('#tvt_wpsfile\:' + str(filenumber) + ' > td:nth-child(1) > div:nth-child(1)')
        erstelldatumfeld = driver.find_element_by_css_selector('#tvt_wpsfile\:' + str(filenumber) + ' > td:nth-child(2) > div:nth-child(1)')
        abrechnungsdatumfeld = driver.find_element_by_css_selector('#tvt_wpsfile\:' + str(filenumber) + ' > td:nth-child(3) > div:nth-child(1)')
        dokumentbezeichnungfeld = driver.find_element_by_css_selector('#tvt_wpsfile\:' + str(filenumber) + ' > td:nth-child(4) > div:nth-child(1)')
    except:
        lastfilefound = True
        break
    # Make the german date format sortable
    datum = datetime.datetime.strptime(datumfeld.text, "%d.%m.%Y").strftime("%Y-%m-%d")
    erstelldatum = datetime.datetime.strptime(erstelldatumfeld.text, "%d.%m.%Y").strftime("%Y-%m-%d")
    abrechnungsdatum = datetime.datetime.strptime(abrechnungsdatumfeld.text, "%d.%m.%Y").strftime("%Y-%m-%d")
    if time.strptime(erstelldatum,"%Y-%m-%d") < oldesterstelldatum:
        break
    # Specify the file name and take care of spaces
    nicefilename = 'HCM_eJ_' + datum + '_' + erstelldatum + '_' + abrechnungsdatum + '_' + dokumentbezeichnungfeld.text + '.pdf'
    nicefilename = nicefilename.replace(' ', '_')
    # Handle duplicate names
    if os.path.isfile(os.path.join(downloadfolder, nicefilename)):
        print('Document found twice!')
        doublefilename = nicefilename
        # If the same document name exists, start numbering by adding _I, _II, _III, etc.
        if not doublefilename.endswith('I.pdf'):
            nicefilename = doublefilename[:-4]+'_I.pdf' #add first 'I'
            os.rename(os.path.join(downloadfolder, doublefilename), os.path.join(downloadfolder, nicefilename)) #change existing filename
        nicefilename = nicefilename[:-4]+'I.pdf' #add 'I' to current (new) file
    # Show user you are still working on it
    print(str(filenumber) + '.)')
    # Open the current payslip & wait
    driver.execute_script("fw_tvt_wpsfile('" + str(filenumber) + "')")
    time.sleep(2)
    assert 'Kein Dokument? Dokument in neuem Fenster öffnen!' in driver.page_source
    # Find the link and click to download
    pdflink = driver.find_element_by_link_text('Kein Dokument? Dokument in neuem Fenster öffnen!')
    # Would have been nice to not be overridden by the Content-Disposition header:
    #driver.execute_script("arguments[0].setAttribute('download',nicefilename)", pdflink)
    #driver.execute_script("arguments[0].setAttribute('target', '_blank')", pdflink)

    pdflink.click()
    time.sleep(random.randint(1,3))
    
    # Get the newest file in the download folder
    mostrecentfile = max([downloadfolder + f for f in os.listdir(downloadfolder)],key=os.path.getctime)
    # Extract the "hash name" without path nor file extension
    hashname = mostrecentfile[len(downloadfolder):len(downloadfolder)+32]
    print(hashname + ' → ' + nicefilename)
    # Give it a nice name
    os.rename(mostrecentfile, os.path.join(downloadfolder, nicefilename))
    # Look for duplicate downloaded files with hash names and erase them
    downloadfiletable = os.listdir(downloadfolder)
    for entry in downloadfiletable:
        if hashname in entry and entry[-4:] == '.pdf':
            print('Removing file ' + str(entry) + '.')
            os.remove(os.path.join(downloadfolder, entry))
    # Go back to overview table on the website
    driver.back()
    filenumber += 1
# Log out when everything is done    

# Close Firefox
time.sleep(random.randint(1,3))
print('Logging out...')
driver.switch_to.default_content()
frames = driver.find_elements_by_tag_name('frame')
activeframe = driver.switch_to.frame(frames[0])
logoutlink = driver.find_element_by_link_text('Abmelden')
logoutlink.click()
driver.quit()
