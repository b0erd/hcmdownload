# hcmdownload
intended for Firefox on Linux/Unix-like operating systems

Downloader for documents from the German HR portal HCM. For private use only, no warranty. Please check your legislation and contract before use!

Please take this script without warranty of any kind. If you don't understand what it does, please don't execute it. This script will remove files on your computer. Please don't blame me if it erases the wrong ones. This script will autonomously log in to the HCM portal. As you are liable for your account and the actions originating from your computer and network, please check the legal situation and applicable contracts before running it.

I am not responsible for any harm this does to you or your computer.

License: http://www.gnu.org/licenses/gpl-3.0.txt

This is a python program. To learn more about python look here: https://www.python.org/
The program works like a puppeteer on Firefox. In order to perform the automation it uses the testing framework Selenium: https://selenium-python.readthedocs.io/

To make it work please install
Python 3
Selenium

For Debian, Ubuntu etc.:
apt update
apt install python3 python3-selenium firefox-geckodriver

For Arch, Manjaro, EndeavourOS:
pacman -Sy
pacman -S python python-selenium geckodriver

You will have to fill the variables with your
credentials before running the script.

To run execute:
python3 hcmdownload.py